#!/usr/bin/env fish
set SRC /home/ao/music/upload
set DEST /home/ao/music/beet
set SINGLES 1.Singles
set PLAYLISTS 2.Playlists

if test -d $SRC/$SINGLES
    for dir in $SRC/$SINGLES/*
        if count $dir/*
            if test -d $DEST/$SINGLES/(basename $dir)
                mv -n $dir/* $DEST/$SINGLES/(basename $dir)
                rmdir $dir
            else
                mv -n $dir $DEST/$SINGLES
            end
        else
            rmdir $dir
        end
    end
    rmdir $SRC/$SINGLES
end

if test -d $SRC/$PLAYLISTS
    for dir in $SRC/$PLAYLISTS/*
        mv -n $dir $DEST/$PLAYLISTS
    end
    rmdir $SRC/$PLAYLISTS
end


for dir in $SRC/*
    if count $dir/*
        if test -d $DEST/(basename $dir)
            mv -n $dir/* $DEST/(basename $dir)
        else
            mv -n $dir $DEST
        end
    else
        rmdir $dir
    end
end

mkdir -p $SRC/$PLAYLISTS $SRC/$SINGLES
