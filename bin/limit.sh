#!/usr/bin/env bash

PERCENT=40

PROGRAM=HandBrakeCLI

while true
do
	PIDS=$(pgrep "$PROGRAM")
	echo PIDS: "$PIDS"

	for pid in $PIDS
	do
		ALREADY_RUNNING=$(ps -eocmd |& grep -c "cpulimit -b -p $pid -l $PERCENT")
		if [ "$ALREADY_RUNNING" -eq 1 ]
		then
			cpulimit -b -p "$pid" -l $PERCENT
		fi
	done
	sleep 30s
done

