#!/usr/bin/env bash

# first: change DIR, HOST and LINK_BASE to your remote upload directory, SSH host and URL.

# upload_that [ -t | -r ] FILE
# defaults with an UUIDv4 inserted in the filename
# -t permits to keep the original filename
# -r removes the distant file. Only works when uploaded with -t

DIR="u/"
HOST="uploader"
LINK_BASE="https://u.ppom.me/"

true_upload=0
rm_upload=0

if [[ "$1" = "-t" ]]
then
    true_upload=1
    shift
fi

if [[ "$1" = "-r" ]]
then
    true_upload=1
    rm_upload=1
    shift
fi

if [[ "" = "$1" || ! -f "$1" ]]
then
    echo "Usage: upload.sh <FILE>"
    exit 1
fi

FILE="$1"
BASE="$(basename "$1")"

if [[ $true_upload = 1 ]]
then
    RFILE="${BASE}"
else
    BASEBASE="${BASE%.*}"
    EXTENSION="${FILE#*.}"
    RFILE="${BASEBASE}-$(uuid -v4).${EXTENSION}"
fi

if [[ $rm_upload = 1 ]]
then
    ssh "$HOST" rm "${DIR}${RFILE}"
    return_code=$?
    if [[ $return_code = 0 ]]
    then
        echo "removed ${LINK_BASE}${RFILE}"
    else
        echo "error removing file."
        exit 1
    fi
else
    OLD_MOD=$(stat -c "%a" "${FILE}")
    chmod 644 "${FILE}"
    rsync -z --progress "${FILE}" "${HOST}:${DIR}${RFILE}"
    return_code=$?
    chmod "${OLD_MOD}" "${FILE}"
    if [[ $return_code = 0 ]]
    then
        echo "${LINK_BASE}${RFILE}" | xclip -selection clipboard
        echo "${LINK_BASE}${RFILE}"
    else
        echo "error uploading file."
        exit 1
    fi
fi

