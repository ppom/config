#!/usr/bin/env bash
SESSION="bg"
# cd

# launch ikhal
# tmux new-session -d -s $SESSION \
#     sh -c "while true; do ikhal; done" || ( echo already running; exit 1 )

# periodically launch vdirsyncer
# press enter to launch now
# tmux new-window -t $SESSION: -n vdirsyncer \
tmux new-session -d -s $SESSION -n vdirsyncer \
    sh -c "vdirsyncer sync; while true; do read -t $((2 * 60 * 60)); date; vdirsyncer sync; date; done"

# Using the unwrapped mpv (`mpvnoscripts`) permits to disable mpv plugins, and not having this mpv instance have a DBUS interface.
# This way it is not stopped when pressing pause. It is "shadowed", in a way.
tmux new-window -t $SESSION: -n mpv \
    mpvnoscripts --loop --volume=1 ~/.local/files/daydream.mp3 2>/dev/null

tmux next-window -t $SESSION
