# ppom config

Here are my personnal scripts and dotfiles. 

I did not make a lot of efforts for compatibility, but you should not have a lot to do to arrange theses for you: most scripts are documented.

These are homemade scripts to run a self-made desktop environment. I use Debian and NixOS.


[bin/](/bin) contains the scripts I use daily. They are documented in [BIN.md](BIN.md).

[commands/](/commands) contains non obvious scripts and commands I want to keep track of. They are self-documented.

[dotfiles/](/dotfiles) contains my config files. They are self-documented.

the [dynamic\_wallpaper directory](/dynamic_wallpaper) contain a system to make and update a wallpaper based on a background image and text file.

[old\_bins/](/old_bins) contains scripts I no longer use.
