#!/usr/bin/env bash
SESSION=bg
cd

#TARGET="$(tmux new-window -P -t $SESSION: -n ssh-kalama ssh kalama -L 5900:kalama:5900 sleep infinity)"
TARGET="$(tmux new-window -P -t $SESSION: -n ssh-kalama ssh kalama -L 5900:kalama:5900)"
tmux split-window -t $TARGET vncviewer localhost -geometry 1366x768+0+0

tmux next-window -t $SESSION
