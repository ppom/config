#!/usr/bin/env fish

if not set -q DISPLAY
    set -e DISPLAY=":0"
end

set -l number (grep -oE '[0-9]' ~/.fehbg)
set -l maxnumber (ls ~/img/wallpapers/bictober | wc -l )
if test $number -lt $maxnumber
    set -g newnumber (math "$number + 1")
else
    set -g newnumber 1
end

echo $number $maxnumber $newnumber

feh --bg-max /home/ao/img/wallpapers/bictober/$newnumber.png
