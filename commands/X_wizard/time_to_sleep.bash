#!/bin/bash
# A script runned when it's late by cron. The screen blinks, telling me to stop geeking.
br=/home/ao/bin/brightness
lastbr=$($br)
highbr=$(($lastbr + 20))
lowbr=$(($lastbr - 20))
for i in {1..3}
do for j in $(seq $highbr -1 $lowbr; seq $lowbr 1 $highbr)
   do $br $j; done
done
$br $lastbr

