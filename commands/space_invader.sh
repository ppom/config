#!/usr/bin/env bash

# Variables globales

DIR="$(mktemp -d)"
HEIGHT="15"
WIDTH="15"

# Initialisation dossier

init_ligne() {
	carac="$1"
	tput setaf 7
	echo -n "{"
	for _ in $(seq "$WIDTH")
	do
		echo -n "$carac"
	done
	echo "}"
}

refresh_screen() {
	for i in $(seq 1 "$HEIGHT")
	do
		tput cup "$i" 0
		init_ligne " "
	done
	for enemy in "$DIR/position/enemies"/*
	do
		enemy="$(basename "$enemy")"
		if test "$enemy" != "*"
		then
			enemy_y="$(echo "$enemy" | cut -d- -f1)"
			enemy_x="$(echo "$enemy" | cut -d- -f2)"
			tput cup "$(( "$enemy_y" + 1 ))" "$(( "$enemy_x" + 1 ))"
			tput setaf 1
			echo 0
		fi
	done
	for shot in "$DIR/position/shots"/*
	do
		shot="$(basename "$shot")"
		if test "$shot" != "*"
		then
			shot_y="$(echo "$shot" | cut -d- -f1)"
			shot_x="$(echo "$shot" | cut -d- -f2)"
			tput cup "$(( "$shot_y" + 1 ))" "$(( "$shot_x" + 1 ))"
			tput setaf 2
			echo ↑
		fi
	done
	tput cup "$(( "$(player_y)" + 1 ))" "$(( "$(player_x)" + 1 ))"
	tput setaf 3
	echo "^"
}

player_x() {
	if test -z "$1"
	then
		cat "$DIR/position/player_x"
	else
		echo "$1" > "$DIR/position/player_x"
	fi
}

player_y() {
	if test -z "$1"
	then
		cat "$DIR/position/player_y"
	else
		echo "$1" > "$DIR/position/player_y"
	fi
}

generate_enemy() {
	echo 2 > "$DIR/position/enemies/0-$(( "$RANDOM" % "$WIDTH" ))"
}

init() {
	mkdir -p "$DIR"/position/enemies "$DIR"/position/shots
	
	# Affichage
	setterm --cursor off
	clear
	init_ligne "-"
	for _ in $(seq "$HEIGHT")
	do
		init_ligne " "
	done
	init_ligne "-"

	# Joueur et Ennemis
	player_x "$(( "$WIDTH" / 2 ))"
	player_y "$(( "$HEIGHT" - 1 ))"
	generate_enemy
}

shoot() {
	touch "$DIR/position/shots/$(( "$(player_y)" - 1 ))-$(player_x)"
	last_shot="$round"
}

move_player() {
	tput cup $(( "$HEIGHT" + 2 )) 0
	read -s -n1 -t0.1 input
	if test "$input" = "j" && test "$(player_x)" -ne "1"
	then
		player_x "$(( "$(player_x)" - 1))"
	elif test "$input" = "k" && test "$(player_x)" -ne "$(( "$WIDTH" - 1 ))"
	then
		player_x "$(( "$(player_x)" + 1))"
	elif test "$input" = "q" && test "$(( "$last_shot" + 4 ))" -lt "$round"
	then
		shoot
	fi
}

move_shots() {
	for shot in "$DIR/position/shots"/*
	do
		shot_pos="$(basename "$shot")"
		if test "$shot_pos" != "*"
		then
			shot_y="$(echo "$shot_pos" | cut -d- -f1)"
			shot_x="$(echo "$shot_pos" | cut -d- -f2)"
			shot_y="$(( "$shot_y" - 1))"
			if test "$shot_y" -eq 0
			then
				rm "$shot"
			elif test -f "$DIR/position/enemies/$shot_y-$shot_x"
			then
				rm "$shot"
				life="$(( "$(cat "$DIR/position/enemies/$shot_y-$shot_x")" - 1 ))"
				if test "$life" -eq 0
				then
					rm "$DIR/position/enemies/$shot_y-$shot_x"
				else
					echo "$life" > "$DIR/position/enemies/$shot_y-$shot_x"
				fi
			else
				mv "$shot" "$DIR/position/shots"/"$shot_y-$shot_x"
			fi
		fi
	done
}

move_enemies() {
	for enemy in "$DIR/position/enemies"/*
	do
		enemy_pos="$(basename "$enemy")"
		enemy_y="$(echo "$enemy_pos" | cut -d- -f1)"
		enemy_x="$(echo "$enemy_pos" | cut -d- -f2)"
		enemy_y="$(( "$enemy_y" + 1))"
		if test "$enemy_y" -ge "$HEIGHT"
		then
			rm "$enemy"
		else
			mv "$enemy" "$DIR/position/enemies"/"$enemy_y-$enemy_x"
		fi
	done
}

init

round=1
last_shot=0
while test ! -f "$DIR/position/enemies/$(player_y)-$(player_x)"
do
	move_player
	move_shots
	if test $(( "$round" % 2 )) -eq 0 
	then
		move_enemies
	fi
	if test $(( "$round" % 1 )) -eq 0 
	then
		generate_enemy
	fi
	refresh_screen
	round=$(( "$round" + 1 ))
done
sleep 0.5

tput cup "$(( "$(player_y)" + 1 ))" "$(( "$(player_x)" + 1 ))"
echo X
tput cup $(( "$HEIGHT" + 2 )) 1
setterm --cursor on
