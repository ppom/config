#!/usr/bin/env xonsh

# Ce script permet de convertir un .password-store sous la forme
# ~/.password-store/folder/website/username en CSV importable par Bitwarden
# C'est du xonsh, mais c'est facile à passer en python, y'a peu de shell dans ce code finalement.

# folder,favorite,type ,name,notes,fields,reprompt,login_uri       ,login_username,login_password,login_totp


import sys
import base64

def utf8_or_base64(thing):
    if type(thing) == str:
        return thing
    try:
        return thing.decode("utf-8")
    except UnicodeDecodeError:
        print("except!")
        return base64.b64encode(thing)

def ifif(obj, key):
    return (obj[key] if key in obj else "") + ","

def printline(obj):
    folder = ifif(obj, "folder")
    favorite = ifif(obj, "favorite")
    type = ","
    name = ifif(obj, "name")
    if not name:
        print(f"no name: {obj}", file=sys.stderr)
        return "FCK"
    notes = ifif(obj, "notes")
    fields = ","
    reprompt = "0,"
    login_uri = ifif(obj, "login_uri")
    login_username = ifif(obj, "login_username")
    login_password = ifif(obj, "login_password")
    return folder + favorite + type + name + notes + fields + reprompt + login_uri + login_username + login_password
    

def main():
    files = $(pass git ls-files | rg ".gpg$").rstrip().split('\n')

    out = []

    print("folder,favorite,type,name,notes,fields,reprompt,login_uri,login_username,login_password,login_totp")

    for entry in files:
        obj = {}
        stripped = entry[:-4]
        names = stripped.split("/")

        content = utf8_or_base64($(pass show @(stripped)))
        content_lines = content.split("\n")
        if len(content_lines) == 0 or len(content.strip()) == 0:
            print(f"empty password: {stripped}", file=sys.stderr)
            
        obj["login_password"] = '"' + content_lines[0].replace('"', '""') + '"'

        obj["notes"] = '"' + "\n".join(content_lines[1:]).replace('"', '""').rstrip() + '"'

        if len(names) > 3:
            new_names = []
            new_names.append('/'.join(names[:len(names)-2]))
            new_names.extend(names[len(names)-2:])
            names = new_names

        if len(names) == 1:
            obj["name"] = stripped
        elif len(names) == 2:
            obj["folder"] = names[0]
            obj["name"] = names[1]
            if "." in names[1]:
                obj["login_uri"] = names[1]
        elif len(names) == 3:
            obj["folder"] = names[0]
            obj["name"] = names[1]
            obj["login_username"] = names[2]
            if "." in names[1]:
                obj["login_uri"] = names[1]

        line = printline(obj)
        try:
            print(line)
        except UnicodeEncodeError:
            print(entry, file=sys.stderr)
            raise Exception("prout")

main()
