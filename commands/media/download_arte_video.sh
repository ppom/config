#!/usr/bin/env bash

exit_error() {
    echo -en '\e[0;31mERROR\e[0m:       ' >&2
    echo "$1" >&2
    remove_directory
    exit 1
}

progress() {
    echo -en '\e[0;33mIN PROGRESS\e[0m: ' >&2
    echo "$1..." >&2
}

ok() {
    echo -en '\e[0;32mDONE\e[0m:        ' >&2
    echo "$1" >&2
}

remove_directory() {
    # rm -r "$DIRNAME"
    echo
}

[ -z "$1" ] &&
    exit_error "No URL specified"

# URL must be something like: https://arte-cmafhls.akamaized.net/am/cmaf/095000/095100/095143-000-A/210426055345/medias/095143-000-A_st_VO-FRA.vtt

URL_JOIN="-A_"
# dev!
VIDEO_FILE=v216.mp4
AUDIO_FILE=aud_VO.mp4
SUB_FILE=st_VO-FRA.vtt
NSUB_FILE=subtitle.srt

DL_FILES="$VIDEO_FILE $AUDIO_FILE $SUB_FILE"
MERGE_FILES="$VIDEO_FILE $AUDIO_FILE $NSUB_FILE"

URL_PREFIX="${1%${URL_JOIN}*}"
DIRNAME="${URL_PREFIX##*/}"

# mkdir $DIRNAME ||
#     exit_error "Could not create directory"

for SUFFIX in $DL_FILES
do
    progress "Downloading $SUFFIX"
    # curl "${URL_PREFIX}${URL_JOIN}${SUFFIX}" -o "$DIRNAME/$SUFFIX" --compressed -H 'Accept: */*' -H 'Accept-Language: en,fr;q=0.7,en-GB;q=0.3' -H 'Referer: https://www.arte.tv/' -H 'Origin: https://www.arte.tv' ||
    #     exit_error "Could not download the necessary files"

    ok "File $SUFFIX"
done

progress "Converting subtitle"

ffmpeg -hide_banner -i "$DIRNAME/$SUB_FILE" "$DIRNAME/$NSUB_FILE"
sed -i 's/^.*,,//' "$DIRNAME/$NSUB_FILE"
dos2unix "$DIRNAME/$NSUB_FILE"

ok "Subtitle converted"

progress "Merging files"

FFMPEG="ffmpeg -hide_banner -c:v copy -c:a copy -c:s mov_text $DIRNAME.mp4"
for FILE in $MERGE_FILES
do
    FFMPEG="$FFMPEG -i $DIRNAME/$FILE"
done

eval $FFMPEG ||
    exit_error "Could not merge files"

ok "Files are merged in here: $DIRNAME.mp4"'\n'"You can remove $DIRNAME/"

remove_directory

exit 0
