package main

import (
	"bufio"
	"fmt"
	// "math"
	"os"
	"path"
	"slices"
	"strings"
	"sync"
	"time"

	// "golang.org/x/exp/maps"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func ret[T any](a T, e error) T {
	if e != nil {
		panic(e)
	}
	return a
}

func parseIfHasPrefix(line, prefix string) (time.Time, bool) {
	if strings.HasPrefix(line, prefix) {
		dateStr := line[1+strings.Index(line, ":"):]
		return ret(time.Parse("20060102T150405", dateStr)), true
	}
	var t time.Time
	return t, false
}

type Dodo [2]time.Time

func parse(path string) *Dodo {
	var start, end time.Time
	var ok, summaryFound bool
	file := ret(os.Open(path))
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if !summaryFound {
			if strings.HasPrefix(line, "SUMMARY") {
				summaryFound = true
			}
		} else if start.IsZero() {
			start, _ = parseIfHasPrefix(line, "DTSTART")
		} else {
			end, ok = parseIfHasPrefix(line, "DTEND")
			if ok {
				return &Dodo{start, end}
			}
		}
	}
	fmt.Fprintf(os.Stderr, "could not parse %v\n", path)
	check(scanner.Err())
	check(file.Close())
	return nil
}

func main() {
	appendWait := make(chan struct{})

	// Add dates to array
	dodosC := make(chan Dodo)
	dodos := make([]Dodo, 0)
	go func() {
		defer func() { appendWait <- struct{}{} }()
		var dodo Dodo
		var ok bool
		for {
			dodo, ok = <-dodosC
			if !ok {
				break
			}
			dodos = append(dodos, dodo)
		}
	}()

	// base dir
	homeDir := ret(os.UserHomeDir())
	dir := path.Join(homeDir, ".local/files/calendars/dodo")

	// Parse files
	var wg sync.WaitGroup
	filesC := make(chan os.DirEntry)
	for i := 0; i < 30; i++ {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			var dirEntry os.DirEntry
			var ok bool
			for {
				dirEntry, ok = <-filesC
				if !ok {
					break
				}
				if dirEntry.IsDir() {
					continue
				}
				if !strings.HasSuffix(dirEntry.Name(), ".ics") {
					continue
				}
				dodo := parse(path.Join(dir, dirEntry.Name()))
				if dodo != nil {
					dodosC <- *dodo
				}
			}
		}(i)
	}

	// Read directory
	files := ret(os.ReadDir(dir))
	for _, file := range files {
		filesC <- file
	}
	close(filesC)
	wg.Wait()
	close(dodosC)

	// Wait
	<-appendWait

	// Sort
	slices.SortFunc(dodos, func(a, b Dodo) int {
		if a[0].After(b[0]) {
			return 1
		}
		if b[0].After(a[0]) {
			return -1
		}
		if a[1].After(b[1]) {
			return 1
		}
		if b[1].After(a[1]) {
			return -1
		}
		return 0
	})

	// CSV
	for _, dodo := range dodos {
		fmt.Printf(
			"%v,%v\n",
			dodo[0].Format("2006-01-02 15:04:05"),
			dodo[1].Format("2006-01-02 15:04:05"),
		)
	}

	// Print for humans
	// for _, dodo := range dodos {
	// 	y, m, d := dodo[0].Date()
	// 	h0, m0, _ := dodo[0].Clock()
	// 	h1, m1, _ := dodo[1].Clock()
	// 	// hd,md_, :=dodo[1].Sub(dodo[0]).Clock()
	// 	delta := dodo[1].Sub(dodo[0])
	// 	hd := int(math.Floor(delta.Hours()))
	// 	md := int(math.Floor(delta.Minutes())) % 60
	// 	fmt.Fprintf(os.Stderr,
	// 		"%v-%02v-%02v from %02v:%02v to %02v:%02v, slept %02v:%02v\n",
	// 		y, int(m), d, h0, m0, h1, m1, hd, md,
	// 	)
	// }

	// Print for computers
	// fmt.Println(string(ret(json.Marshal(dodos))))

	// Stats
	// minuteSums := make(map[string]int)
	// for _, dodo := range dodos {
	// 	y, m, d := dodo[0].Date()
	// 	// TODO should split at 21:30
	// 	minuteSums[fmt.Sprintf("%v-%02v-%02v", y, int(m), d)] += int(math.Floor(dodo[1].Sub(dodo[0]).Minutes()))
	// }
	// fmt.Println(minuteSums)
	// days := maps.Keys(minuteSums)
	// slices.Sort(days)
	// for _, day := range days {
	// 	fmt.Printf("%v → %02v:%02v\n", day, minuteSums[day]/60, minuteSums[day]%60)
	// }
	// values := maps.Values(minuteSums)
	// min := slices.Min(values)
	// max := slices.Max(values)
	// fmt.Printf("min: %02v:%02v, max: %02v:%02v", min/60, min%60, max/60, max%60)
}
