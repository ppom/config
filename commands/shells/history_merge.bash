#!/usr/bin/env bash
# vim:ft=bash
#
# history_merge.bash
#
# Merge .bash_history and the in-memory history together, and saving in back
# in-memory and/or back into the history file.
# This is basically a 'merged write' that allows you to merge and share the
# current in-memory history between multiple terminal sessions, when you are
# ready to share it.
#
#
# These are the history setup I have in my ".bash_profile"
#
#   # History Aliases...
#
#   # merge and write history
#   hm() { source ~/bin/history_merge.bash; }
#
#   # Replace in-memory history with written history
#   hr() { if [[ -n $HISTFILE ]]
#          then  history -c; history -r; echo "history read (replace)";
#          else echo "history is disabled";
#          fi }
#
#   # Just write the in-memory history to the history file
#   hw() { if [[ -n $HISTFILE ]]
#          then  history -w; echo "history write (no merge)";
#          else echo "history is disabled";
#          fi }
#
#   # disable history file save
#   hd() { unset HISTFILE; }
#
# And in my ".bash_logout" I have
#
#   # merge and clean history before exiting
#   hm
#   hd  # already saved, no need to save again
#
####
#
# Anthony Thyssen,   8 October 2020
# Paco Pompeani,     9 June 2021
#

# DO not merge if history save has been disabled for some reason
if [[ -e "$HISTFILE" ]]; then

  # create an exclusive lock on the history file
  exec {history_lock}<"$HISTFILE"
  flock $history_lock

  # Temporary file for processing
  histtmp=$( mktemp "${TMPDIR:-/tmp}/bash_history_merge.XXXXXXXXXX" )

  nl < "$HISTFILE" | sort -k2 -k 1,1nr | uniq -f1 | sort -n | cut -f2 > "$histtmp"

  # Select one or both of the following actions...

  # OPTIONAL: Replace in-memory history with merged history
  history -c
  history -r "$histtmp"

  # OPTIONAL: Replace current $HISTFILE with merged version
  cp "$histtmp" "$HISTFILE"

  # Explicitly release the lock
  exec {history_lock}<&-

  # Clean-up, shred file if posible
  shred -u "$histtmp" || rm "$histtmp"

  unset histtmp

fi

