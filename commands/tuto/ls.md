Deux choses à comprendre, parce que sans ça, en effet, ça ne fait pas sens du tout :sweat_smile:

## 1. `ls`

#### Comportement par défaut

`ls` affiche un fichier par ligne quand il détecte que la sortie n'est pas un terminal.
→ Quand il est dans un script ou dans un `$()`, c'est comme si tu mettais l'option `-1`.

#### Options d'affichage

Tu peux changer la façon dont s'affichent les résultats avec `ls` (et forcer un *behavior*) avec les options `--format`.

#### Demander à `ls` de *quote* les fichiers qui contiennent des *whitespaces*

Si tu veux tout sur la même ligne, je te conseille `-Q`.

## 2. `bash`

#### Séparation des arguments

Ce qu'il faut bien comprendre en shell, c'est la différence entre
- *`bash` découpe les arguments et les donne à `echo`*
- et *`bash` donne tout d'un bloc et `echo` affiche un bloc*

`echo Paco va te coucher` et `echo 'Paco va te coucher'` vont afficher le même résultat, mais dans le premier cas, `echo` reçoit 4 arguments qu'il va concaténer par des espaces. Dans le 2e cas, `echo` va recevoir un seul argument qu'il va afficher tel quel.

`echo Paco   va te coucher`
- arguments que bash donne à echo : `["Paco", "va", "te", "coucher"]`
- *output* : `Paco va te coucher`
`echo "Paco   va te coucher"`
- argument que bash donne à echo : `["Paco   va te coucher"]`
- *output* : `Paco   va te coucher`

## 3. Ton problème

Là aussi, c'est une *quote issue*. Quand tu fais `echo $a`, bash sépare les arguments selon les *whitespaces*, dont `\n` fait partie.
Donc `echo` ne reçoit pas de retours à la ligne en argument et affiche tout sur la même ligne.

Si tu veux le "vrai" résultat, donc avec les retours à la ligne (puisque `ls` affiche un fichier par ligne en mode "script") :
Il faut tout donner à `echo` en un seul argument, donc il faut écrire un de ces trucs :
```
a="$(ls)" # quotes optionnelles ici
echo "$a" # mais nécessaires ici
```
```
echo "$(ls)"
```

Pour usage dans un script, je déconseille de mettre tout sur une même ligne + `ls -Q`.
Le plus efficace c'est d'avoir un fichier par ligne et de `pipe`r ça avec un `xargs '-d\n'`
*(ou `xargs -d'\n'`, ce qui revient au même pour bash)*
L'option `-d` de xargs permet de changer le délimiteur, qui est par défaut n'importe quel *whitespace* il me semble

#### Exemple concret

```
/tmp/tmp.GhkJGYzlxj $ \ls
L.Effondrement.S01E01.mkv  L.Effondrement.S01E03.mkv  L.Effondrement.S01E05.mkv  L.Effondrement.S01E07.mkv        README.txt
L.Effondrement.S01E02.mkv  L.Effondrement.S01E04.mkv  L.Effondrement.S01E06.mkv  L.Effondrement.S01E08.FiNAL.mkv

/tmp/tmp.GhkJGYzlxj $ \ls *.mkv | xargs '-d\n' -I{} echo ffmpeg -i {} {}.mp4
ffmpeg -i L.Effondrement.S01E01.mkv L.Effondrement.S01E01.mkv.mp4
ffmpeg -i L.Effondrement.S01E02.mkv L.Effondrement.S01E02.mkv.mp4
ffmpeg -i L.Effondrement.S01E03.mkv L.Effondrement.S01E03.mkv.mp4
ffmpeg -i L.Effondrement.S01E04.mkv L.Effondrement.S01E04.mkv.mp4
ffmpeg -i L.Effondrement.S01E05.mkv L.Effondrement.S01E05.mkv.mp4
ffmpeg -i L.Effondrement.S01E06.mkv L.Effondrement.S01E06.mkv.mp4
ffmpeg -i L.Effondrement.S01E07.mkv L.Effondrement.S01E07.mkv.mp4
ffmpeg -i L.Effondrement.S01E08.FiNAL.mkv L.Effondrement.S01E08.FiNAL.mkv.mp4
```

#### Avec `find`

```
/tmp/tmp.GhkJGYzlxj $ find -name '*.mkv' -exec echo ffmpeg -i {} {}.mp4 ';'
ffmpeg -i ./L.Effondrement.S01E08.FiNAL.mkv ./L.Effondrement.S01E08.FiNAL.mkv.mp4
ffmpeg -i ./L.Effondrement.S01E07.mkv ./L.Effondrement.S01E07.mkv.mp4
ffmpeg -i ./L.Effondrement.S01E06.mkv ./L.Effondrement.S01E06.mkv.mp4
ffmpeg -i ./L.Effondrement.S01E05.mkv ./L.Effondrement.S01E05.mkv.mp4
ffmpeg -i ./L.Effondrement.S01E04.mkv ./L.Effondrement.S01E04.mkv.mp4
ffmpeg -i ./L.Effondrement.S01E03.mkv ./L.Effondrement.S01E03.mkv.mp4
ffmpeg -i ./L.Effondrement.S01E02.mkv ./L.Effondrement.S01E02.mkv.mp4
ffmpeg -i ./L.Effondrement.S01E01.mkv ./L.Effondrement.S01E01.mkv.mp4
```

On note que le `';'`, *quot*é, n'est pas interprété par `bash` et est passé en argument à `find`. On trouve souvent `\;`, mais je trouve ça moins explicite.

#### `fd` *>* POSIX `find`

Si tu veux faire un truc dans le genre de l'exemple au-dessus en passant par un utilitaire moderne et pratique, mais qui n'est pas dans GNU coreutils :

```
/tmp/tmp.GhkJGYzlxj $ fd . -e mkv -x echo ffmpeg -i {} {.}.mp4
ffmpeg -i L.Effondrement.S01E06.mkv L.Effondrement.S01E06.mp4
ffmpeg -i L.Effondrement.S01E07.mkv L.Effondrement.S01E07.mp4
ffmpeg -i L.Effondrement.S01E08.FiNAL.mkv L.Effondrement.S01E08.FiNAL.mp4
ffmpeg -i L.Effondrement.S01E03.mkv L.Effondrement.S01E03.mp4
ffmpeg -i L.Effondrement.S01E04.mkv L.Effondrement.S01E04.mp4
ffmpeg -i L.Effondrement.S01E02.mkv L.Effondrement.S01E02.mp4
ffmpeg -i L.Effondrement.S01E01.mkv L.Effondrement.S01E01.mp4
ffmpeg -i L.Effondrement.S01E05.mkv L.Effondrement.S01E05.mp4
```

On note que tout est plus court à taper et qu'on a gagné `{.}` qui donne est remplacé par le nom du fichier sans l'extension. (Il y a aussi ceux-là `{/} {//} {.} {/.}`, le `man`)
On note aussi que `fd` parallélise, ce qui n'est pas toujours voulu

