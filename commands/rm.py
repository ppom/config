# Remove duplicate files, keeping files which have `text_to_keep` in their filepath
# fd -tf -x sha1sum {} | sort | python rm.py

import os
import fileinput

last_sum = ""
text_to_keep = "good_directory"
deleted = 0

for line in fileinput.input():

    sum = line.split(" ")[0]
    file = "." + ".".join(line.split(".")[1:]).strip()

    if sum == last_sum:
        deleted += 1
        print(f"sum: {sum} deleting ", end="")
        if text_to_keep in last_file:
            os.remove(file)
        else:
            os.remove(last_file)

    last_sum = sum
    last_file = file

print(f"deleted {deleted} files")
