# 'todo wallpaper' with background image

This very simple script takes an image and a text file. It generates an image with both inputs and sets it as wallpaper.

## Usage

### Parameters

The script `draw.sh` does the job.
Edit the first lines of the file to set the parameters in accordance with your setup:

| Parameter     | Description                                                                                                                                                        |
| ------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| TODOFILE      | The path to the text file you want to draw                                                                                                                         |
| WALLPAPERFILE | The path to the image file you want to draw                                                                                                                        |
| OUTFILE       | The generated file's path. Replace it with `TEMPFILE=$(mktemp)` if you want it to be temporary.                                                                    |
| DISPLAY       | The DISPLAY variable is not present in the cron environment. Run `echo DISPLAY='$'{\$DISPLAY:-\'$DISPLAY\'}` in a terminal window to see what you should put here. |
| WALLSIZE      | The size of your screen. Run `xrandr` to view it.                                                                                                                  |
| TEXTCOLOR     | The text color. See ImageMagick's documentation to see available colors.                                                                                           |
| FONTFAMILY    | The text font family. See ImageMagick's documentation to see available fonts.                                                                                      |
| FONTSIZE      | The text font size. See ImageMagick's documentation to see available sizes.                                                                                        |

### Cron Schedule

I made a `Makefile` to improve the usability of the script.
Running `make` will run the script only if the text file or the image file was updated.

You'll have to copy the 3 file parameters of the script in the `Makefile` for it to behave consistently.

This way, you can create a cron job to update the wallpaper every minute without rebuilding the temporary image if not necessary:
`crontab -e` will open a file where you can put this line at the end:

```cron
*    *    *    *    *     make --quiet -C $PATH_TO_THIS_FOLDER
```

replacing `$PATH_TO_THIS_FOLDER` by the path to the folder where the script and the Makefile are.

## Requirements

This script uses ImageMagick's `convert` command and the wallpaper setter (and image viewer) `feh`.
You can get them on a distribution using `apt` by running this:

```
sudo apt install imagemagick feh
```

This script also assumes you use a bare window manager, openbox or another desktop environment which doesn't paint over the X root window.

See [this stackoverflow post](https://askubuntu.com/questions/66914/how-to-change-desktop-background-from-command-line-in-unity)
for alternatives to the command-line `feh` utility for the GNOME Desktop (it's easy).

If you use the `Makefile`, you'll need the `make` utility. It's included by default in most distributions.

If you make a cron job, you'll need a cron daemon. It's included by default in most distributions.
