#!/bin/sh

# Requires ImageMagick's `convert` command.

## Config
TODOFILE=~/todo
WALLPAPERFILE=~/img/wallpapers/wallpica2.png
OUTFILE=~/.cache/wallpaper.png
WALLSIZE=1920x1080
TEXTCOLOR=white
FONTFAMILY="Noto-Mono" 
FONTSIZE=18

## Code
TEMPFILE=$(mktemp)

echo 'text 40,40 "' > "$TEMPFILE"
cat "$TODOFILE" >> "$TEMPFILE"
echo '"' >> "$TEMPFILE"

convert "$WALLPAPERFILE" -size "$WALLSIZE" -font "$FONTFAMILY" -pointsize "$FONTSIZE" -fill "$TEXTCOLOR" -draw "@$TEMPFILE" "$OUTFILE"

export DISPLAY=${DISPLAY:-':0'}
feh --bg-center "$OUTFILE"
