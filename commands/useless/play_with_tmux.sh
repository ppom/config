TMUX_MODIFIER=q

BOUCLE=1
if [[ q != "q$1" ]]
then
    BOUCLE="$1"
fi

setxkbmap us
xdotool key ctrl+q colon
xdotool type 'bind u resize-pane -R 1'
xdotool key Return

while [[ $((BOUCLE > 0)) -eq 1 ]]
do
    xdotool key --delay 15ms ctrl+$TMUX_MODIFIER quotedbl ctrl+$TMUX_MODIFIER K K K K K K ctrl+l \
                ctrl+$TMUX_MODIFIER percent ctrl+$TMUX_MODIFIER H H H H H H H H H H H H H H H H H H H H H ctrl+$TMUX_MODIFIER u ctrl+l
    BOUCLE=$((BOUCLE - 1))
done
setxkbmap fr
