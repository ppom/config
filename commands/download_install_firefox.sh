#!/usr/bin/env bash
VERSION=89.0.2
BASENAME=firefox-$VERSION.tar.bz2
DOWNLOAD_URL=https://download-installer.cdn.mozilla.net/pub/firefox/releases/$VERSION/linux-x86_64/en-US/$BASENAME
DEST_DIR=/opt/firefox-upstream
DESKTOP_FILE=/usr/share/applications/firefox-upstream.desktop

# colors
RED="\e[1;31m"
GREEN="\e[1;32m"
BOLD="\e[0;1m"
CLEAR="\e[0m"

function exit_die {
    echo
    echo -e "${RED}Error:${CLEAR} $1"
    exit 1
}

function ok {
    echo -e " ${GREEN}Ok${CLEAR}."
}

function doing {
    printf "${BOLD}$1${CLEAR}"
}

doing "Installation des outils nécessaires... (en tant qu'administrateur) (nécessite de taper le mot de passe puis Entrée. Rien ne s'affiche quand on tape le mot de passe c'est normal)"
sudo apt install -y curl &>/dev/null
[[ $? -eq 0 ]] || exit_die "N'a pas pu installer les outils nécessaires. Tu as bien tapé le mot de passe ?"
ok

doing "Création d'un dossier temporaire..."
TMP_DIR=$(mktemp -d)
[[ $? -eq 0 ]] || exit_die "Création du dossier impossible. Disque dur plein ?"
ok
cd "$TMP_DIR"

doing "Téléchargement de la dernière version de Firefox...\n"
curl "$DOWNLOAD_URL" -o "$BASENAME"
[[ $? -eq 0 ]] || exit_die "Téléchargement échoué"
ok

doing "Extraction de l'archive..."
tar -xjf "$BASENAME"
[[ $? -eq 0 ]] || exit_die "Ne peut pas télécharger l'archive !"
ok

if [[ -e "$DEST_DIR" ]]
then
	doing "Suppression du dossier précédent... (en tant qu'administrateur)"
	sudo rm -r "$DEST_DIR"
	[[ $? -eq 0 ]] || exit_die "Ne peut pas supprimer l'ancien dossier !"
	ok
fi

doing "Déplacement du dossier firefox vers $DEST_DIR ... (en tant qu'administrateur)"
sudo mv firefox "$DEST_DIR"
[[ $? -eq 0 ]] || exit_die "Ne peut pas déplacer le dossier !"
ok

doing "Création de l'entrée Firefox dans le menu des applications (en tant qu'administrateur)"
cat > entry.desktop << EOF
[Desktop Entry]
Categories=Network;WebBrowser;
Comment=Browse the World Wide Web
Exec=$DEST_DIR/firefox %U
GenericName=Web Browser
Icon=$DEST_DIR/browser/chrome/icons/default/default128.png
MimeType=text/html;text/xml;application/xhtml+xml;application/vnd.mozilla.xul+xml;x-scheme-handler/http;x-scheme-handler/https;x-scheme-handler/ftp
Name=Firefox Upstream
Terminal=false
Type=Application
EOF
[[ $? -eq 0 ]] || exit_die "Ne peut pas créer l'entrée ! (1)"
sudo cp entry.desktop "$DESKTOP_FILE"
[[ $? -eq 0 ]] || exit_die "Ne peut pas créer l'entrée ! (2)"
ok

doing "Création d'un lien vers l'application pour que Firefox soit exécutable depuis le terminal aussi :) (en tant qu'administrateur)"
sudo ln -s $DEST_DIR/firefox /usr/bin/firefox-upstream
[[ $? -eq 0 ]] || exit_die "Ne peut pas créer le lien !"
ok

doing "Installation de firefox effectuée avec succès.\n"
