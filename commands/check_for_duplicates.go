package main

import (
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"sync"
)

type File struct {
	path     string
	checksum string
	fileinfo fs.FileInfo
}

type Dir struct {
	fullname string
	dirs     map[string]*Dir
	files    map[string]*File
	Similar  map[*Dir]int
}

func NewDir(fullname string) *Dir {
	return &Dir{fullname, make(map[string]*Dir), make(map[string]*File), make(map[*Dir]int)}
}

func (d *Dir) Add(file File) {
	path := strings.Split(file.path, "/")
	if len(path) == 1 {
		dot := make([]string, 2)
		dot = append(dot, ".")
		path = append(dot, path...)
	}
	d.add(file, path[1:len(path)-1])
}

func (d *Dir) add(file File, path []string) {
	if len(path) == 0 {
		d.files[file.fileinfo.Name()] = &file
		return
	}
	dirname := path[0]
	if _, ok := d.dirs[dirname]; !ok {
		d.dirs[dirname] = NewDir(filepath.Join(d.fullname, dirname))
	}
	d.dirs[dirname].add(file, path[1:])
}

func (d *Dir) GetDir(filepath string) *Dir {
	path := strings.Split(filepath, "/")
	return d.getDir(path[1 : len(path)-1])
}

func (d *Dir) getDir(path []string) *Dir {
	if len(path) == 0 {
		return d
	}
	dirname := path[0]
	if _, ok := d.dirs[dirname]; !ok {
		return nil
	}
	return d.dirs[dirname].getDir(path[1:])
}

func (d *Dir) Print() {
	d.print("")
}

func (d *Dir) print(tab string) {
	fmt.Fprintf(os.Stderr, "%v%v : %v dirs, %v files\n", tab, d.fullname, len(d.dirs), len(d.files))
	for _, dir := range d.dirs {
		dir.print(tab + "  ")
	}
}

type FilesByChecksum map[string][]string

func Checksum(path string) (string, error) {
	f, err := os.Open(path)
	if err != nil {
		return "", err
	}
	defer f.Close()

	hasher := sha1.New()
	if _, err := io.Copy(hasher, f); err != nil {
		return "", err
	}
	value := hex.EncodeToString(hasher.Sum(nil))
	return value, nil
}

func CountFiles(directory string) int {
	nb := 0
	filepath.Walk(directory, func(path string, info fs.FileInfo, err error) error {
		if !info.IsDir() {
			nb++
			fmt.Fprintf(os.Stderr, "\rCounting files... %v", nb)
		}
		return nil
	})
	fmt.Fprintln(os.Stderr)
	return nb
}

func AllFiles(directory string) chan File {
	intra := make(chan File)
	ret := make(chan File)
	var wg sync.WaitGroup
	for i := 0; i < runtime.NumCPU(); i++ {
		wg.Add(1)
		go func() {
			var err error
			var file File
			ok := true
			for ok {
				file, ok = <-intra
				if ok {
					file.checksum, err = Checksum(file.path)
					if err != nil {
						fmt.Fprintf(os.Stderr, "checksum failed on %v : %v\n", file.path, err)
					}
					ret <- file
				}
			}
			wg.Done()
		}()
	}
	go func() {
		filepath.Walk(directory, func(path string, info fs.FileInfo, err error) error {
			if err != nil {
				fmt.Fprintf(os.Stderr, "read failed on %v : %v\n", path, err)
				return nil
			}
			if !info.IsDir() {
				intra <- File{path, "", info}
			}
			return nil
		})
		close(intra)
	}()
	go func() {
		wg.Wait()
		close(ret)
	}()
	return ret
}

func Indexes(dirname string, files chan File) (*Dir, FilesByChecksum) {
	dir := NewDir(dirname)
	fc := make(FilesByChecksum)
	nb := 0
	for file := range files {
		nb++
		fmt.Fprintf(os.Stderr, "\x1b[2K\rCalculating checksums... %v : %v", nb, file.path)
		dir.Add(file)
		fc[file.checksum] = append(fc[file.checksum], file.path)
	}
	fmt.Fprintln(os.Stderr)
	return dir, fc
}

func (dir *Dir) SimilarDirectories(filesByChecksum FilesByChecksum) {
	nb := 0
	for _, files := range filesByChecksum {
		nb += len(files)
		fmt.Fprintf(os.Stderr, "\x1b[2K\rCalculating duplicated files... %v", nb)
		if len(files) > 1 {
			dirs := make([]*Dir, 0)
			for _, file := range files {
				filedir := dir.GetDir(file)
				if filedir == nil {
					fmt.Fprintf(os.Stderr, "Unregistered file (should not happen) : %v\n", file)
				} else {
					dirs = append(dirs, filedir)
				}
			}
			for _, dir := range dirs {
				for _, otherdir := range dirs {
					if dir != otherdir {
						dir.Similar[otherdir]++
					}
				}
			}
		}
	}
	fmt.Fprintln(os.Stderr)
}

func (d *Dir) PrintSimilarDirectories() {
	for dir, nb := range d.Similar {
		if nb > 0 && d.fullname < dir.fullname {
			fmt.Printf("%v\t%v\t%v\n", nb, d.fullname, dir.fullname)
		}
	}
	for _, dir := range d.dirs {
		dir.PrintSimilarDirectories()
	}
}

func main() {
	if len(os.Args) < 2 {
		fmt.Fprintln(os.Stderr, "First argument must be a directory")
		os.Exit(1)
	}
	directory := os.Args[1]
	_ = CountFiles(directory)
	files := AllFiles(directory)
	dir, filesByChecksum := Indexes(directory, files)
	dir.SimilarDirectories(filesByChecksum)
	dir.PrintSimilarDirectories()
}
