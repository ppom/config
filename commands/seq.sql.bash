for table in oc_calendar_appt_bookings oc_calendar_appt_configs oc_calendar_invitations oc_calendar_reminders oc_calendar_resources oc_calendar_resources_md oc_calendar_rooms oc_calendar_rooms_md oc_calendarchanges oc_calendarobjects oc_calendarobjects_props oc_calendars oc_calendarsubscriptions oc_cards oc_cards_properties oc_cospend_bill_owers oc_cospend_bills oc_cospend_categories oc_cospend_currencies oc_cospend_members oc_cospend_paymentmodes oc_cospend_projects oc_cospend_shares oc_deck_assigned_labels oc_deck_assigned_users oc_deck_attachment oc_deck_board_acl oc_deck_boards oc_deck_cards oc_deck_labels oc_deck_stacks
do
	max="$(sudo -u nextcloud psql nextcloud -c "SELECT MAX(id) FROM $table ;" | sed -n 3p | tr -d ' ')"
	if test -n "$max"
	then
		new_sequence_start=$((max + 1))
		sudo -u nextcloud psql nextcloud -c "ALTER SEQUENCE ${table}_id_seq START $new_sequence_start; ALTER SEQUENCE ${table}_id_seq RESTART;"
	fi
done
