// Change the contrast of a fish theme file
// Usage : go run contrast.go COEF FILESRC FILEOUT
package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func main() {
	coef, _ := strconv.ParseFloat(os.Args[1], 32)
	file := os.Args[2]
	newfile := os.Args[3]

	input, _ := os.Open(file)
	scanner := bufio.NewScanner(input)
	output := ""
	hexreg := regexp.MustCompile("^[a-fA-F0-9]{6}$")
	hex2f := func(s string) float64 {
		r, _ := strconv.ParseInt(s, 16, 32)
		return float64(r)
	}
	f2hex := func(f float64) string {
		s := strconv.FormatUint(uint64(f), 16)
		if len(s) > 2 {
			s = "ff"
		} else if len(s) == 0 {
			s = "00"
		} else if len(s) == 1 {
			s = "0" + s[0:1]
		}
		fmt.Println(s, len(s))
		return s[len(s)-2:]

	}

	for scanner.Scan() {
		line := scanner.Text()
		if !strings.HasPrefix(line, "fish_color_") {
			output += line + "\n"
		} else {
			splitLine := strings.Split(line, " ")
			if len(splitLine) != 2 || !hexreg.MatchString(splitLine[1]) {
				output += line + "\n"
			} else {
				r, g, b := hex2f(splitLine[1][0:2]), hex2f(splitLine[1][2:4]), hex2f(splitLine[1][4:6])
				// if r+g+b < 126*3 {
				// 	coef = 1 / coef
				// }
				r, g, b = r*coef, g*coef, b*coef
				hex := f2hex(r) + f2hex(g) + f2hex(b)
				output += splitLine[0] + " " + hex + "\n"
			}
		}
	}
	os.WriteFile(newfile, []byte(output), 0644)
}
