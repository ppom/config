## 1

read
```json
{
    "filter": {
        "transaction_associee": {
            "_eq": "{{ $trigger.key }}"
        }
    }
}
```

sum
```js
module.exports = async function(data) {
    let somme = 0;
    for (const item of data.$last) {
        somme += item.valeur;
    }
    somme *= 100;
    somme = Math.round(somme);
    somme /= 100;
	return somme;
}
```

write
```json
{
    "somme": "{{ $last }}"
}
{
    "filter": {
        "id": {
            "_eq": "{{ $trigger.key }}"
        }
    }
}
```

## 2

read
```json
{
    "filter": {
        "id": {
            "_eq": "{{ $trigger.keys[0] }}"
        }
    }
}
```

read
```json
{
    "filter": {
        "transaction_associee": {
            "_eq": "{{ $last[0].transaction_associee }}"
        }
    }
}
```

sum
```js
module.exports = async function(data) {
    let somme = 0;
    for (const item of data.$last) {
        somme += item.valeur;
    }
    somme *= 100;
    somme = Math.round(somme);
    somme /= 100;
	return somme;
}
```

```json
{
    "somme": "{{ $last }}"
}
{
    "filter": {
        "id": {
            "_eq": "{{ recuperer_transaction_du_mouvement[0].transaction_associee }}"
        }
    }
}
```
