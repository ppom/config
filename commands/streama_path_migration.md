# Migration de dossier streama

Faire un remplacement de /data/movies par /data/streama/movies sur la table FILE.

```sql
-- Replace a directory path
UPDATE FILE
SET
LOCAL_FILE = REGEXP_REPLACE(LOCAL_FILE, '/data/movies/', '/data/streama/movies/')
WHERE
LOCAL_FILE IS NOT NULL
;
```

Dans la console admin :
Faire un remplacement de l'upload directory de /data/uploads vers /data/streama/uploads
Faire un remplacement de Local Video Files de /data/movies vers /data/streama/movies
