# Scripts

Here is an overview of the scripts I use on a regular basis.

Scripts are written in bash or fish.

## alert

Send a desktop notification and starts a Psy Trance track.

```bash
$ sleep 30m && alert
$ very_long_command; alert
```

requires:

- `mpv`
- `notify-send`
- a Desktop Environment or a notification daemon running

## backup_to

Backup my files

```bash
$ backup_to /mnt/2022-10-26/
```

requires `rsync`

## bomber.sh

Connect to my bluetooth speaker

requires `bluetoothctl`

## colors

Display terminal 256 colors. A script made by Tom Hale.

```bash
$ colors
# beautiful colors here
```

## deepl

Wrapper around [deepl-translate-cli](https://github.com/Omochice/deepl-translate-cli).
Retrieve and inject the personal token.

You must specify language codes as arguments, then give the sentence to translate as stdin and close it.

```bash
$ deepl fr en
Je t'aime
# CTRL-D
# or
$ echo "Je t'aime" | deepl fr en
```

requires `deepl-translate-cli`

## extract_mkv_subtitles.sh

Extract subtitles from all `*.mkv` files in the current directory and subdirectories.
Downloaded script.

```bash
$ extract_mkv_subtitles.sh
```

requires:

- `mkvmerge`
- `mkvextract`

## limit.sh

Limit all processus' of a specified program to a specified CPU use percentage

```bash
# Adjust PERCENT and PROGRAM in the script
$ limit.sh
```
requires:

- `cpulimit`
- `pgrep`

## magnet2torrent

A magnet URL must be in the clipboard.
Use a HTTP webservice to convert the magnet URL to a `.torrent` file.
Then copy it to a remote server.

```bash
$ magnet2torrent <optional-name-for-uploaded-file>
```

requires:

- `curl`
- `rsync`

## status_*

Those scripts are used by the following [conky script](dotfiles/conkyrc).
This system creates a status line like this:
```
♪Octave - Delph  ⚙15 ℝ9 ♪36 ✹45 🖫91 ⚡91▲ 20m 🐳🐳 ☭ ▂▄▆█ 20/10 20:10
```

- Octave, from Delph playing
- 15% CPU usage
- 9% RAM usage
- 36% volume of main pulseaudio channel
- 45% screen brightness
- 91% disk usage
- battery at 91%, charging complete in 20 minutes.
- 2 docker containers running
- VPN connected
- Wifi signal 4/4
- date is 2022/10/20, time is 20:10

- `status_battery` requires `acpi`
- `status_bluetooth` requires `bluetoothctl`
- `status_docker` requires `docker`
- `status_internet` requires `ping`, `nmcli`
- `status_sound` requires `amixer`

## superstarter

Launch all the applications and scripts I need on a standard desktop session.
Fills password prompts.

```bash
# usually launched with a shortcut
$ superstarter
```

requires:

- `nohup`
- `pgrep`
- `xdotool`
- `rbw`
- `xclip`

## toggle_layout

Toggle the X11 layout from bépo to azerty (and vice versa).

```bash
# usually launched with a shortcut
$ toggle_layout
```

requires `setxkbmap`

## upload_that.sh

Upload a file to a remote server then copy generated URL to clipboard and print it.

```bash
$ upload_that.sh file.pdf
https://u.ppom.me/file-<uuid>.pdf

$ upload_that.sh -t file.pdf
https://u.ppom.me/file.pdf
```

requires:

- `rsync`
- `uuid`
- `ssh`
- `xclip`

## volume_fix.sh

Launch in a tmux session mpv looping on an audio file at an inaudible volume.
Useful to suppress noise in the JACK output when no other audio is playing.

requires:

- `tmux`
- `mpv` (here aliased `mpvnoscripts`, adjust it)
