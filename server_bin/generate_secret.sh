#!/usr/bin/env bash

# Very oriented script to generate a secret

if [[ -z "$1" ]]
then
	echo no secret name
	exit 1
fi
SECRET_LOCATION="/var/secrets/$1.secret"

if [[ -z "$2" ]]
then
	LENGTH=20
else
	case "$2" in
	    *[!0-9]*) echo length must be a number; exit 1;;
	    *) LENGTH=$2;;
	esac
fi

PASSWD=$(head /dev/urandom | tr -dc '[:alnum:]' | sed 's/^\(.\{'"$LENGTH"'\}\).*$/\1/')
HASHWD=$(mkpasswd -m sha-512 "$PASSWD")

echo "$HASHWD" > "$SECRET_LOCATION"

echo "PASSWD=$PASSWD"
echo "HASHWD=$HASHWD"
echo "SECRET_LOCATION=$SECRET_LOCATION"
