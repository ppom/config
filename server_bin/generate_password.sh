#!/usr/bin/env bash

# Very oriented script to generate a password

LENGTH=20
PASSWD=$(head /dev/urandom | tr -dc '[:alnum:]' | sed 's/^\(.\{'"$LENGTH"'\}\).*$/\1/')
echo "$PASSWD"
