#!/usr/bin/env bash

TOKEN=$(cat /var/secrets/funkwhale/musicImportToken)
LIBRARY=307d7f12-23df-49c1-9394-e39daf94047c

warn() {
	echo "warn: $1" >&2
}

check_file() {
	file "$1" | grep -q MPEG || file "$1" | grep -q ".mp3: Audio file"
	ret=$?
	test $ret != 0 && warn "$1 is not an MP3 file"
	return $ret
}

upload_file() {
	realfile=$(readlink -f "$1")
	check_file "$realfile" && \
	printf "$1: " && \
	curl 'https://music.ppom.me/api/v1/uploads/' \
		-s -w "%{http_code}" -o /dev/null \
		-X POST \
		-H "Authorization: Bearer $TOKEN" \
		-F library=$LIBRARY \
		-F audio_file=@"$realfile" && \
	echo
}

test -z "$1" && warn "must pass files and directories as parameters"

while test -n "$1"
do
	realfile=$(readlink -f "$1")
	if test -d "$realfile"
	then
		for file in "$realfile"/*
		do
			upload_file "$file"
		done
	else
		upload_file "$realfile"
	fi
	shift
done
