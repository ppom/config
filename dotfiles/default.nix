{ stdenv, lib, fetchgit
, acpi
, alsa-utils
, bluetoothctl
, clementine
, coreutils
, cpulimit
, espeak
, gawk
, gnupg
, inetutils
, mkvtoolnix
, network-manager
, nvidia-settings
, openssh # scp
, pass
, pulseaudio
, rofi
, scrot
, wget
, xclip
, xdg-utils
, xdotool
}:

let
  url = "https://framagit.org/ppom/config";
in
stdenv.mkDerivation {
  name = "ppom_config";
  src = ./.;

  installPhase = ''
    mkdir $out $out/bin
    cp bin/* $out/bin
  '';

  outputs = [ "out" ];

  meta = with lib; {
    description = "ppom's dotfiles and shell files";
    homepage = url;
    license = licenses.gpl3;
    maintainers = with maintainers; [];
  };
}
