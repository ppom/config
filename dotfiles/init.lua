-- Following of this configuration file:
-- https://framagit.org/ppom/nixos/-/blob/main/modules/common/init.lua

-- Vimtex
vim.g.tex_flavor = 'latex'

-- Escape mappings
vim.keymap.set('i', '<c-c>', '<esc>')
vim.keymap.set('i', '²', '<esc>')

-- leader mappings
vim.g.mapleader = ' '

-- copy to system clipboard
vim.keymap.set('n', '<leader>a', 'ggVG')
vim.keymap.set('n', '<leader>=', 'gg=G')
vim.keymap.set('v', '<leader>y', '"+y')

-- vimrc edition
vim.api.nvim_create_user_command("Vimrc", [[tabe ~/.config/nvim/init.lua]], { force = true })
vim.api.nvim_create_autocmd('BufWritePost', {
	pattern = vim.fs.normalize("~/.config/nvim/init.lua"),
	command = "source %",
	group = vim.api.nvim_create_augroup('vimrc-edition', {})
})

-- Sharing registers across instances
vim.api.nvim_create_autocmd({ 'CursorHold', 'TextYankPost', 'FocusGained', 'FocusLost' }, {
	command = "if exists(':rshada') | rshada | wshada | endif",
	group = vim.api.nvim_create_augroup('share_shada', { clear = true })
})

-- Remember folds
local remember_folds = vim.api.nvim_create_augroup('remember_folds', { clear = true })
vim.api.nvim_create_autocmd('BufWinLeave', { group = remember_folds, command = "silent! mkview" })
vim.api.nvim_create_autocmd('BufWinEnter', { group = remember_folds, command = "silent! loadview" })

-- Show space line endings
vim.cmd.match([[WarningMsg /\s\+$/]])

-- Create digraphs!
vim.cmd.digraphs("lc 128557")

local indentations = vim.api.nvim_create_augroup('remember_folds', { clear = true })
vim.api.nvim_create_autocmd('FileType',
	{
		pattern = 'html,htmldjango,javascript,typescript',
		group = indentations,
		command =
		"set tabstop=2 shiftwidth=2 softtabstop=2"
	})
vim.api.nvim_create_autocmd('FileType',
	{ pattern = 'jsonnet', group = indentations, command = "set autoindent expandtab tabstop=2 shiftwidth=2" })

vim.api.nvim_create_autocmd({'BufRead','BufNewFile'},
	{ pattern = '*.twig', group = indentations, command = "set ft=htmldjango" })

vim.api.nvim_create_autocmd({'BufRead','BufNewFile'},
	{ pattern = '*.tpl.html', group = indentations, command = "set ft=php" })

-- local dictionnary = {}
-- dictionnary["fr-FR"] = { ":/home/ao/.config/ltex/common.txt", ":/home/ao/.config/ltex/fr.txt" }
-- dictionnary["en-US"] = { ":/home/ao/.config/ltex/common.txt", ":/home/ao/.config/ltex/en.txt" }
-- dictionnary["en"] = dictionnary["en-US"]
-- require 'lspconfig'.ltex.setup {
-- 	autostart = false,
-- 	settings = {
-- 		ltex = {
-- 			dictionnary = dictionnary
-- 		}
-- 	}
-- }

-- quirks
-- vim.api.nvim_create_autocmd('LspAttach', {
-- 	callback = function(ev)
-- 		local yeswiki, _ = string.find(vim.cmd.pwd(), "yeswiki")
-- 		print("yeswiki:", yeswiki)
-- 		if yeswiki then
-- 			vim.api.nvim_del_augroup_by_name('UserLspConfig')
-- 		end
-- 	end
-- })

-- Smart folding
-- vim.api.nvim_create_autocmd({ "FileType" }, {
--   callback = function()
--     if require("nvim-treesitter.parsers").has_parser() then
--       vim.opt.foldmethod = "expr"
--       vim.opt.foldexpr = "nvim_treesitter#foldexpr()"
--     else
--       vim.opt.foldmethod = "syntax"
--     end
--   end,
-- })
