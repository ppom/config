function store
set url (echo "$argv[1]" | base64 | tr -d ' \n' | tr / ' ')
curl "$argv[1]" | jq | tee tests/$url
end
