function p
  if ping -c1 -W3 212.27.40.241 >/dev/null 2>&1
    echo ping ok
    if ping -c1 -W3 dns2.proxad.net &>/dev/null 2>&1
        echo dns ok
        return 0
    end
    echo no dns
    return 2
  end
  echo no ping
  return 1
end
