function diary
    if test -L ~/.password-store
        rm -f ~/.password-store
    end
    if not test -e ~/.password-store
        ln -s ~/.diary-store ~/.password-store
        return 0
    end
    return 1
end
