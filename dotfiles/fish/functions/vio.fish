function vio
    for arg in $argv
        nvim --server /tmp/nvim.sock --remote-tab (realpath $arg)
    end
end
