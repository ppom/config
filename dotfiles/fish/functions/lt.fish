function lt --argument lang to_lang word
    set -l base_url "https://libretranslate.de"
    set -l content_type "Content-Type: application/x-www-form-urlencoded"

    curl -X POST "$base_url/translate" -H "$content_type" --data-urlencode "q=$word" --data-urlencode "source=$lang" --data-urlencode "target=$to_lang" 2>/dev/null | jq -r ".translatedText"
end
