function wr --argument lang word
    set -l ua "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:99.0) Gecko/20100101 Firefox/99.0" 
    set -l pup_filter '#articleWRD table:first-of-type .ToWrd json{}' 
    set -l jq_filter  '.[].text|select(length > 0)'

    set -l query (string split " " $word | string join "+")
    curl -A $ua https://www.wordreference.com/$lang/$query 2>/dev/null | pup $pup_filter | jq -r $jq_filter
end
