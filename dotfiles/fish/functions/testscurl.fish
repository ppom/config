function testscurl
 for file in tests/*
set url (echo (basename $file) | tr ' ' / | base64 -d)
echo $url
set tmpfile (mktemp)
curl $url | jq >> $tmpfile
if test $status -ne 0
echo curl failed
return
end
if test (cat $file | sha1sum) != (cat $tmpfile | sha1sum)
echo different json
diff $file $tmpfile
return
end
echo ok
end
end
