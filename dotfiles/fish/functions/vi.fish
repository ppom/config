function vi --wraps=vim
    set sock /tmp/nvim.sock
    if not test -e $sock
        nvim --listen $sock $argv
    else
        nvim $argv
    end
end
