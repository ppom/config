if status is-interactive

    # Go in sway!
    if not pgrep sway >/dev/null 2>&1
        mkdir -p ~/.local/share/sway
        exec sway &>> ~/.local/share/sway/(date '+%y-%m-%d_%H:%M:%S.md').log
    end

    # Go in tmux!
    if command -v tmux >/dev/null 2>&1; and test -z "$TMUX"; and test -z "$SSH_TTY"
        tmux new -A -s 0
    end

    set -g fish_prompt_pwd_dir_length 20

    # I don't know how to override this with `functions/l.fish` so I do it here
    alias ls exa
    alias l exa
    alias t trash
    alias mv 'mv -i'
    alias cp 'cp -i'

    alias bépo "setxkbmap fr"
    alias azer "setxkbmap fr bepo"

    # Go to sleep
    alias dodo poweroff

    # Go up in the history, then
    # Press ^G to delete it from history
    bind -M insert \cg forget

    # Don't want `?` to be a glob
    set -U fish_features qmark-noglob

    # Launch ssh-agent
    if not pgrep -U (id -u) ssh-agent &>/dev/null
        ssh-agent &>/dev/null
    end
    set -gx SSH_AUTH_SOCK (fd -ts -o (id -u) --full-path '/tmp/ssh-.*/agent.*' /tmp/)

    set -x GOPATH $HOME/.go
    set -x ANSIBLE_VAULT_PASSWORD_FILE ~/.ssh/ansible_vault

    set -x PICASOFT_SSH_USER ppom
    set -x PICASOFT_SSH_KEY_PATH ~/.ssh/picasoft
end
