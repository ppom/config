# Print an optspec for argparse to handle cmd's options that are independent of any subcommand.
function __fish_reaction_global_optspecs
	string join \n h/help
end

function __fish_reaction_needs_command
	# Figure out if the current invocation already has a command.
	set -l cmd (commandline -opc)
	set -e cmd[1]
	argparse -s (__fish_reaction_global_optspecs) -- $cmd 2>/dev/null
	or return
	if set -q argv[1]
		# Also print the command, so this can be used to figure out what it is.
		echo $argv[1]
		return 1
	end
	return 0
end

function __fish_reaction_using_subcommand
	set -l cmd (__fish_reaction_needs_command)
	test -z "$cmd"
	and return 1
	contains -- $cmd[1] $argv
end

complete -c reaction -n "__fish_reaction_needs_command" -s h -l help -d 'Print help (see more with \'--help\')'
complete -c reaction -n "__fish_reaction_needs_command" -f -a "start" -d 'Start reaction daemon'
complete -c reaction -n "__fish_reaction_needs_command" -f -a "show" -d 'Show current matches and actions'
complete -c reaction -n "__fish_reaction_needs_command" -f -a "flush" -d 'Remove a target from reaction (e.g. unban)'
complete -c reaction -n "__fish_reaction_needs_command" -f -a "test-regex" -d 'Test a regex'
complete -c reaction -n "__fish_reaction_needs_command" -f -a "help" -d 'Print this message or the help of the given subcommand(s)'
complete -c reaction -n "__fish_reaction_using_subcommand start" -s c -l config -d 'configuration file in json, jsonnet or yaml format. required' -r -F
complete -c reaction -n "__fish_reaction_using_subcommand start" -s l -l loglevel -d 'minimum log level to show' -r
complete -c reaction -n "__fish_reaction_using_subcommand start" -s s -l socket -d 'path to the client-daemon communication socket' -r -F
complete -c reaction -n "__fish_reaction_using_subcommand start" -s h -l help -d 'Print help'
complete -c reaction -n "__fish_reaction_using_subcommand show" -s s -l socket -d 'path to the client-daemon communication socket' -r -F
complete -c reaction -n "__fish_reaction_using_subcommand show" -s f -l format -d 'how to format output: json or yaml' -r -f -a "{json\t'',yaml\t''}"
complete -c reaction -n "__fish_reaction_using_subcommand show" -s l -l limit -d 'only show items related to this STREAM[.FILTER]' -r
complete -c reaction -n "__fish_reaction_using_subcommand show" -s h -l help -d 'Print help (see more with \'--help\')'
complete -c reaction -n "__fish_reaction_using_subcommand flush" -s s -l socket -d 'path to the client-daemon communication socket' -r -F
complete -c reaction -n "__fish_reaction_using_subcommand flush" -s f -l format -d 'how to format output: json or yaml' -r -f -a "{json\t'',yaml\t''}"
complete -c reaction -n "__fish_reaction_using_subcommand flush" -s l -l limit -d 'only show items related to this STREAM[.FILTER]' -r
complete -c reaction -n "__fish_reaction_using_subcommand flush" -s h -l help -d 'Print help (see more with \'--help\')'
complete -c reaction -n "__fish_reaction_using_subcommand test-regex" -s c -l config -d 'configuration file in json, jsonnet or yaml format. required' -r -F
complete -c reaction -n "__fish_reaction_using_subcommand test-regex" -s h -l help -d 'Print help (see more with \'--help\')'
complete -c reaction -n "__fish_reaction_using_subcommand help; and not __fish_seen_subcommand_from start show flush test-regex help" -f -a "start" -d 'Start reaction daemon'
complete -c reaction -n "__fish_reaction_using_subcommand help; and not __fish_seen_subcommand_from start show flush test-regex help" -f -a "show" -d 'Show current matches and actions'
complete -c reaction -n "__fish_reaction_using_subcommand help; and not __fish_seen_subcommand_from start show flush test-regex help" -f -a "flush" -d 'Remove a target from reaction (e.g. unban)'
complete -c reaction -n "__fish_reaction_using_subcommand help; and not __fish_seen_subcommand_from start show flush test-regex help" -f -a "test-regex" -d 'Test a regex'
complete -c reaction -n "__fish_reaction_using_subcommand help; and not __fish_seen_subcommand_from start show flush test-regex help" -f -a "help" -d 'Print this message or the help of the given subcommand(s)'
