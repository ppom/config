# Connect

Sometimes sound is strange when you don't *trust* devices.
between `pair` and `connect`, you should issue the `trust` command on a Bluetooth MAC address.

# Microphone and Sound quality

Two bluetooth profile are optimised for different things.
One is better for listening quality but have no microphone.
Can be set graphically with `pavucontrol` (in *Configuration*) or `pactl`:
```
pactl set-card-profile bluez_card.<MACADDRESS> headset-head-unit-msbc
pactl set-card-profile bluez_card.<MACADDRESS> a2dp-sink-sbc
```
